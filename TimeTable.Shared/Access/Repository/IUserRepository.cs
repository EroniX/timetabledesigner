﻿using EroniX.Core.DataAccess;
using TimeTableDesigner.Shared.Entity.Database;

namespace TimeTableDesigner.Shared.Access.Repository
{
    public interface IUserRepository : IEntityRepository<User>
    {
    }
}
