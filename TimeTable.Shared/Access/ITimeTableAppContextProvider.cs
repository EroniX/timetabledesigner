﻿using EroniX.Core;

namespace TimeTableDesigner.Shared.Access
{
    public interface ITimeTableAppContextProvider : IAppContextProvider
    {
    }
}
