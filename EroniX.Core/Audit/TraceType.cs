﻿namespace EroniX.Core.Audit
{
    public enum TraceType
    {
        Enter,
        Leave,
        Message,
        Exception
    }
}
